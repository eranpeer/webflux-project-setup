Feature: Search request validation

  Scenario: cql query param is missing
    Given a search request without cql query param
    When run search
    Then http response status is "BAD_REQUEST"
    And http error message is "Missing required query param cql"

