Feature: Logic of routing requests to monolith or SnS

  Scenario: route request with tenant id that starts with M to monolith
    Given an hello_tenant monolith
    And a search request with tenant id "MiMi"
    When run search
    Then http response status is "OK"
    And search response is "hello MiMi"

