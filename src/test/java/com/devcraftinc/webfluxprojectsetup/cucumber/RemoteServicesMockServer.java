package com.devcraftinc.webfluxprojectsetup.cucumber;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockWebServer;
import org.jetbrains.annotations.NotNull;

import javax.annotation.PreDestroy;
import java.io.IOException;

public class RemoteServicesMockServer {

    private final MockWebServer webServer = new MockWebServer();

    public RemoteServicesMockServer() throws IOException {
        webServer.start();
    }

    public String serverBaseUrl(){
        return webServer.url("").toString();
    }

    @PreDestroy
    private void stopWebServer() throws IOException {
        webServer.shutdown();
    }

    public void setDispatcher(@NotNull Dispatcher dispatcher) {
        webServer.setDispatcher(dispatcher);
    }
}
