package com.devcraftinc.webfluxprojectsetup.cucumber;

import com.devcraftinc.webfluxprojectsetup.MonolithSearch;
import reactor.core.publisher.Mono;

public class HelloFromMonolithSearch implements MonolithSearch {
    @Override
    public Mono<SearchResponse> search(SearchRequest searchRequest) {
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setCqlQuery(searchRequest.getCqlQuery());
        searchResponse.setTenantId(searchRequest.getTenantId());
        searchResponse.setSearchResult("hello from monolith");
        return Mono.just(searchResponse);
    }
}
