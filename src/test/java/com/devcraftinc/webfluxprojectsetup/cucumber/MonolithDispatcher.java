package com.devcraftinc.webfluxprojectsetup.cucumber;

import com.devcraftinc.webfluxprojectsetup.MonolithSearch;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;

public class MonolithDispatcher extends Dispatcher {

    MonolithSearch fakedSearch;

    public MonolithDispatcher(MonolithSearch fakedSearch) {
        this.fakedSearch = fakedSearch;
    }

    @NotNull
    @Override
    public MockResponse dispatch(@NotNull RecordedRequest recordedRequest) throws InterruptedException {
        String path = recordedRequest.getRequestUrl().uri().getPath();

        if (!path.equals("/search"))
            return new MockResponse().setResponseCode(HttpStatus.NOT_FOUND.value());

        MonolithSearch.SearchRequest request = new MonolithSearch.SearchRequest();
        request.setTenantId(recordedRequest.getHeader("tenant-id"));
        request.setCqlQuery(recordedRequest.getRequestUrl().queryParameter("cql"));

        MonolithSearch.SearchResponse response = fakedSearch.search(request).block();

        return toMockResponse(response);
    }

    @NotNull
    private MockResponse toMockResponse(MonolithSearch.SearchResponse response) {
        return new MockResponse().setResponseCode(HttpStatus.OK.value()).setHeader("Content-Type", "application/json").setBody(toJson(response));
    }

    public void setFakedSearch(MonolithSearch fakedSearch) {
        this.fakedSearch = fakedSearch;
    }

    private String toJson(Object response) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
