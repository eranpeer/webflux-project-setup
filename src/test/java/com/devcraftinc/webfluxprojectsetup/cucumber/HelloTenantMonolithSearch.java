package com.devcraftinc.webfluxprojectsetup.cucumber;

import com.devcraftinc.webfluxprojectsetup.MonolithSearch;
import reactor.core.publisher.Mono;

public class HelloTenantMonolithSearch implements MonolithSearch {
    @Override
    public Mono<SearchResponse> search(SearchRequest searchRequest) {
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setCqlQuery(searchRequest.getCqlQuery());
        searchResponse.setTenantId(searchRequest.getTenantId());
        searchResponse.setSearchResult("hello " + searchRequest.getTenantId());
        return Mono.just(searchResponse);
    }
}
