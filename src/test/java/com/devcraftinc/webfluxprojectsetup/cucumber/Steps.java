package com.devcraftinc.webfluxprojectsetup.cucumber;

import com.devcraftinc.webfluxprojectsetup.MonolithSearch;
import cucumber.api.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.PostConstruct;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class Steps {

    @Autowired
    WebTestClient httpClient;

    @Autowired
    MonolithDispatcher monolithDispatcher;

    private WebTestClient.RequestHeadersSpec<?> searchRequestWithoutCqlQueryParam;

    private WebTestClient.RequestHeadersSpec<?> searchRequest;
    private WebTestClient.ResponseSpec searchResponse;
    private WebTestClient.RequestHeadersSpec<?> validSearchRequest;

    @PostConstruct
    void init(){
        validSearchRequest = makeSearchRequest("some_tenant");

        searchRequestWithoutCqlQueryParam = httpClient.get()
                .uri("/search")
                .accept(MediaType.APPLICATION_JSON)
                .header("tenant-id","123")
                .header("Authorization", generateValidToken());
    }

    private WebTestClient.RequestHeadersSpec<?> makeSearchRequest(String tenantId) {
        return httpClient.get()
                .uri("/search?cql=valid_search")
                .accept(MediaType.APPLICATION_JSON)
                .header("tenant-id", tenantId)
                .header("Authorization", generateValidToken());
    }

    @Given("a search request without cql query param")
    public void aSearchRequestWithoutCqlQueryParam() {
        searchRequest = searchRequestWithoutCqlQueryParam;
    }

    @NotNull
    private String generateValidToken() {
        return "valid token";
    }

    @When("run search")
    public void runSearch() {
        searchResponse = searchRequest.exchange();
    }

    @Then("http response status is {string}")
    public void httpResponseStatusIs(String expectedHttpStatus) {
        searchResponse.expectStatus().isEqualTo(HttpStatus.valueOf(expectedHttpStatus));
    }

    @Given("an hello_tenant monolith")
    public void anHello_tenantMonolith() {
        monolithDispatcher.setFakedSearch(new HelloTenantMonolithSearch());
    }

    @Then("search response is {string}")
    public void searchResponseIs(String expectedSearchResult) {
        MonolithSearch.SearchResponse monolithResponse = searchResponse.expectBody(MonolithSearch.SearchResponse.class).returnResult().getResponseBody();
        assertEquals(expectedSearchResult, monolithResponse.getSearchResult());
    }

    @Given("a search request with tenant id {string}")
    public void aSearchRequestWithTenantId(String tenantId) {
        searchRequest = makeSearchRequest(tenantId);
    }

    public static class HttpErrorResponse {
        public String message;
    }

    @Then("http error message is {string}")
    public void httpErrorMessageIs(String expectedHttpErrorMessage) {
        assertEquals(expectedHttpErrorMessage, searchResponse.expectBody(HttpErrorResponse.class).returnResult().getResponseBody().message);
    }

}
