package com.devcraftinc.webfluxprojectsetup.cucumber;

import com.devcraftinc.webfluxprojectsetup.MonolithProperties;
import com.devcraftinc.webfluxprojectsetup.MonolithSearch;
import io.cucumber.java.en.Given;
import okhttp3.mockwebserver.Dispatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = "spring.main.allow-bean-definition-overriding=true"
)
@AutoConfigureWebTestClient(timeout = "1000000") //1000 seconds
public class SpringBootTestConfig {

    @Given("never invoked")
    public void neverInvoked() {
    }

    @TestConfiguration
    public static class AcceptanceTestsConfig {

        @Bean
        RemoteServicesMockServer remoteServicesMockServer(Dispatcher dispatcher) throws IOException {
            RemoteServicesMockServer mockServer = new RemoteServicesMockServer();
            mockServer.setDispatcher(dispatcher);
            return mockServer;
        }

        @Bean
        MonolithDispatcher monolithDispatcher(@Qualifier("remoteMonolithSearch") MonolithSearch monolithSearch){
            return new MonolithDispatcher(monolithSearch);
        }

        @Lazy
        @Bean("remoteMonolithSearch")
        MonolithSearch remoteMonolithSearch(){
            return new HelloFromMonolithSearch();
        }

        @Bean("monolithProperties")
        MonolithProperties monolithProperties(RemoteServicesMockServer mockWireMock){
            return new MonolithProperties(){
                @Override
                public String getBaseUrl() {
                    return mockWireMock.serverBaseUrl();
                }
            };
        }
    }

    @Test
    public void silence(){}
}
