package com.devcraftinc.webfluxprojectsetup.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"classpath:spec"})
public class SpecTestsRunner {
}
