package com.devcraftinc.webfluxprojectsetup;

import reactor.core.publisher.Mono;

public interface MonolithSearch {

    class SearchRequest {
        private String cqlQuery;
        private String tenantId;

        public String getCqlQuery() {
            return cqlQuery;
        }

        public void setCqlQuery(String cqlQuery) {
            this.cqlQuery = cqlQuery;
        }

        public String getTenantId() {
            return tenantId;
        }

        public void setTenantId(String tenantId) {
            this.tenantId = tenantId;
        }
    }

    class SearchResponse {
        private String cqlQuery;
        private String tenantId;
        private String searchResult;

        public String getCqlQuery() {
            return cqlQuery;
        }

        public void setCqlQuery(String cqlQuery) {
            this.cqlQuery = cqlQuery;
        }

        public String getTenantId() {
            return tenantId;
        }

        public void setTenantId(String tenantId) {
            this.tenantId = tenantId;
        }

        public String getSearchResult() {
            return searchResult;
        }

        public void setSearchResult(String searchResult) {
            this.searchResult = searchResult;
        }
    }

    Mono<SearchResponse> search(SearchRequest searchRequest);
}
