package com.devcraftinc.webfluxprojectsetup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    @Qualifier("monolithSearch")
    MonolithSearch monolithSearch;

    @Bean
    RouterFunction router() {
        HandlerFunction<? extends ServerResponse> handler = this::handler;
        return RouterFunctions.route(GET("/search").and(accept(MediaType.APPLICATION_JSON)), handler);
    }

    @Bean("monolithProperties")
	MonolithProperties monolithProperties(){
		return new MonolithProperties();
	}

	@Bean("monolithSearch")
    MonolithSearch monolithSearch(@Value("#{monolithProperties.baseUrl}") String monolithPaseUrl) {
        return new MonolithSearchImpl(monolithPaseUrl);
    }

    private Mono<ServerResponse> handler(ServerRequest serverRequest) {
        if (!serverRequest.queryParam("cql").isPresent())
            throw new ServerWebInputException("Missing required query param cql");

        MonolithSearch.SearchRequest searchRequest = new MonolithSearch.SearchRequest();
        searchRequest.setCqlQuery(serverRequest.queryParam("cql").get());
        List<String> tenantIdHeader = serverRequest.headers().header("tenant-id");
        searchRequest.setTenantId(tenantIdHeader.get(0));

        return monolithSearch
				.search(searchRequest)
                .flatMap(sr -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(
                        Mono.just(sr), MonolithSearch.SearchResponse.class));
    }

}
