package com.devcraftinc.webfluxprojectsetup;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class MonolithSearchImpl implements MonolithSearch {

    private String monolithBaseUrl;

    public MonolithSearchImpl(String monolithBaseUrl) {
        this.monolithBaseUrl = monolithBaseUrl;
    }

    @Override
    public Mono<SearchResponse> search(SearchRequest searchRequest) {
        return WebClient.builder()
                .baseUrl(monolithBaseUrl).build()
                .get().uri("/search?cql=" + searchRequest.getCqlQuery())
                .header("tenant-id",searchRequest.getTenantId())
                .exchange()
                .flatMap(cr-> {
                            System.out.println(cr);
                            return cr.bodyToMono(SearchResponse.class);
                        }
                );
    }
}
